Info from https://medium.com/@aaronverones/using-the-7-1-sass-scss-pattern-with-angular-7-bb210c015dcc

Create src/styles directory.

Remove src/styles.scss (could have keep it and use it instead of src/styles/main.scss).

Create src/styles/main.scss.

Update angular.json, replace "src/styles.scss" with "src/styles/main.scss".

No need to create src/styles/components/ and src/styles/pages/ folders (angular 
    has component specific stylesheets).

No need to create src/styles/vendors/ folder because external stylesheets and scripts 
    should be included in angular.json (under projects/<project name>/architect/build/options).

Create abstracts, base, layout, themes under src/styles/ folder.

Create sass partial files as needed (eg. abstracts/_variables.scss, _functions.scss, .. etc).

Import partial files from main.scss.

Import scss files from component specific scss files. We can use relative path 
    (eg @import "../styles/abstracts/variables"), 
    but we can add src/styles/ to stylePreprocessorOptions in angular.json, and we can use non-relative paths
    (eg. @import "abstracts/variables")

Done    